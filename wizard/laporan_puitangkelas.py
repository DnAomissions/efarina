# -*- coding: utf-8 -*-

from odoo import api, models, fields, _

import logging, datetime

_logger = logging.getLogger(__name__)

class LaporanPiutangKelas(models.TransientModel):
    _name = 'efarina.laporan.piutangkelas'

    def _default_kelas(self):
        return self.env['efarina.kelas'].search([]).ids

    kelas_ids = fields.Many2many('efarina.kelas', string="Kelas", required=True)

    # kelas_id = fields.Many2one("efarina.kelas", 'Kelas', required=True)

    @api.model
    def year_selection(self):
        year = 2015 # replace 2015 with your a start year
        year_list = []
        while year != 2035: # replace 2035 with your end year
            year_list.append((str(year), str(year)))
            year += 1
        return year_list

    start_year = fields.Selection(
        year_selection,
        string="Tahun Awal", required=True,
        default=str(datetime.datetime.today().year), # as a default value it would be 2021
    )

    start_month = fields.Selection([
        ('1', 'January'), 
        ('2', 'February'), 
        ('3', 'March'), 
        ('4', 'April'),
        ('5', 'May'), 
        ('6', 'June'), 
        ('7', 'July'), 
        ('8', 'August'), 
        ('9', 'September'), 
        ('10', 'October'), 
        ('11', 'November'), 
        ('12', 'December')
    ], 'Bulan Awal', required=True, default=str(datetime.datetime.today().month))

    end_year = fields.Selection(
        year_selection,
        string="Tahun Akhir", required=True,
        default=str(datetime.datetime.today().year), # as a default value it would be 2021
    )

    end_month = fields.Selection([
        ('1', 'January'), 
        ('2', 'February'), 
        ('3', 'March'), 
        ('4', 'April'),
        ('5', 'May'), 
        ('6', 'June'), 
        ('7', 'July'), 
        ('8', 'August'), 
        ('9', 'September'), 
        ('10', 'October'), 
        ('11', 'November'), 
        ('12', 'December')
    ], 'Bulan Akhir', required=True, default=str(datetime.datetime.today().month))

    def export_xlsx(self):
        return self.env['efarina.siswa'].export_piutangkelas_xlsx(self.kelas_ids.ids, self.start_year, self.start_month, self.end_year, self.end_month)