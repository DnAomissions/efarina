# -*- coding: utf-8 -*-

from odoo import api, models, fields, _

import logging, datetime

_logger = logging.getLogger(__name__)

class LaporanOrderDay(models.TransientModel):
    _name = 'efarina.laporan.orderday'

    date_start = fields.Date("Tanggal Awal", default=datetime.datetime.today(), required=True)
    date_end = fields.Date("Tanggal Akhir", default=datetime.datetime.today(), required=True)

    def export_xlsx(self):
        return self.env['efarina.order'].export_orderday_xlsx(self.date_start, self.date_end)