# -*- coding: utf-8 -*-

from odoo import api, models, fields, _

import logging, datetime
from dateutil.relativedelta import relativedelta

_logger = logging.getLogger(__name__)

class LaporanPenagihanSiswa(models.TransientModel):
    _name = 'efarina.laporan.penagihansiswa'

    semester = fields.Selection([
        ('I', 'I'),
        ('II', 'II'),
        ('III', 'III'),
        ('IV', 'IV'),
        ('V', 'V'),
        ('VI', 'VI')
    ], 'Semester', required=True)
    siswa_ids = fields.Many2many('efarina.siswa', string='Siswa', required=True)

    perihal = fields.Char('Perihal', required=True, default="Tagihan Administrasi")
    keterangan_bank = fields.Text('Keterangan Bank', default="No Rekening BRI : 0113.01.000.606.30.0 An Yayasan Efarina (wa 082272556685)")

    @api.model
    def year_selection(self):
        year = 2015 # replace 2015 with your a start year
        year_list = []
        while year != 2035: # replace 2035 with your end year
            year_list.append((str(year), str(year)))
            year += 1
        return year_list

    start_year = fields.Selection(
        year_selection,
        string="Tahun Awal", required=True,
        default=str(datetime.datetime.today().year), # as a default value it would be 2021
    )

    start_month = fields.Selection([
        ('1', 'January'), 
        ('2', 'February'), 
        ('3', 'March'), 
        ('4', 'April'),
        ('5', 'May'), 
        ('6', 'June'), 
        ('7', 'July'), 
        ('8', 'August'), 
        ('9', 'September'), 
        ('10', 'October'), 
        ('11', 'November'), 
        ('12', 'December')
    ], 'Bulan Awal', required=True, default=str(datetime.datetime.today().month))

    end_year = fields.Selection(
        year_selection,
        string="Tahun Akhir", required=True,
        default=str(datetime.datetime.today().year), # as a default value it would be 2021
    )
    
    end_month = fields.Selection([
        ('1', 'January'), 
        ('2', 'February'), 
        ('3', 'March'), 
        ('4', 'April'),
        ('5', 'May'), 
        ('6', 'June'), 
        ('7', 'July'), 
        ('8', 'August'), 
        ('9', 'September'), 
        ('10', 'October'), 
        ('11', 'November'), 
        ('12', 'December')
    ], 'Bulan Akhir', required=True, default=str(datetime.datetime.today().month))

    @api.onchange('start_month', 'start_year')
    def _onchange_start(self):
        if self.start_year and self.start_month:
            date = datetime.date(int(self.start_year), int(self.start_month), 1)
            date += relativedelta(months=5)
            self.end_month = str(date.month)
            self.end_year = str(date.year)
    
    @api.onchange('end_month', 'end_year')
    def _onchange_end(self):
        if self.end_year and self.end_month:
            date = datetime.date(int(self.end_year), int(self.end_month), 1)
            date -= relativedelta(months=5)
            self.start_month = str(date.month)
            self.start_year = str(date.year)

    def export_xlsx(self):
        return self.env['efarina.siswa'].export_penagihansiswa_xlsx(self.semester, self.siswa_ids.ids, self.start_year, self.start_month, self.end_year, self.end_month, self.perihal, self.keterangan_bank)