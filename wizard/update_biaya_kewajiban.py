# -*- coding: utf-8 -*-

import datetime

from odoo import models, fields, api, _

class WizardUpdateBiayaKewajiban(models.TransientModel):
    _name = 'wizard.update.biaya.kewajiban'
    _description = 'Wizard Update Biaya Kewajiban'

    kelas_ids = fields.Many2many('efarina.kelas', string="Kelas", required=True)

    biaya_id = fields.Many2one('efarina.biaya', 'biaya', required=True)

    @api.onchange('biaya_id')
    def _onchange_biaya(self):
        if self.biaya_id:
            self.jumlah = self.biaya_id.jumlah

    def _get_default_currency_id(self):
        return self.env.company.currency_id.id

    jumlah = fields.Monetary('Jumlah', default=0)
    currency_id = fields.Many2one('res.currency', 'Currency', default=_get_default_currency_id, required=True)

    type = fields.Selection(string="Jenis", related="biaya_id.type")

    @api.model
    def year_selection(self):
        year = 2015 # replace 2015 with your a start year
        year_list = []
        while year != 2035: # replace 2035 with your end year
            year_list.append((str(year), str(year)))
            year += 1
        return year_list

    year = fields.Selection(
        year_selection,
        string="Year",
        default=str(datetime.datetime.today().year), # as a default value it would be 2021
    )

    month = fields.Selection([
        ('1', 'January'), 
        ('2', 'February'), 
        ('3', 'March'), 
        ('4', 'April'),
        ('5', 'May'), 
        ('6', 'June'), 
        ('7', 'July'), 
        ('8', 'August'), 
        ('9', 'September'), 
        ('10', 'October'), 
        ('11', 'November'), 
        ('12', 'December')
    ], 'Bulan', default=str(datetime.datetime.today().month))

    def action(self):
        for kelas_id in self.kelas_ids:
            kewajiban_ids = self.env['efarina.kelas.kewajiban']

            if self.biaya_id.type == 'yearly':
                kewajiban_ids = kelas_id.kewajiban_ids.filtered(lambda x: x.biaya_id.id == self.biaya_id.id and x.year == self.year)
            elif self.biaya_id.type == 'monthly':
                kewajiban_ids = kelas_id.kewajiban_ids.filtered(lambda x: x.biaya_id.id == self.biaya_id.id and x.month == self.month and x.year == self.year)
            else:
                kewajiban_ids = kelas_id.kewajiban_ids.filtered(lambda x: x.biaya_id.id == self.biaya_id.id)
            
            if kewajiban_ids:
                kewajiban_ids.write({
                    'jumlah': self.jumlah
                })

            kewajiban_siswa_ids = self.env['efarina.siswa.kewajiban']
            siswa_ids = self.env['efarina.siswa'].search([('kelas_id', '=', kelas_id.id)])

            if siswa_ids:
                for siswa_id in siswa_ids:
                    if self.biaya_id.type == 'yearly':
                        kewajiban_siswa_ids = siswa_id.kewajiban_ids.filtered(lambda x: x.biaya_id.id == self.biaya_id.id and x.year == self.year)
                    elif self.biaya_id.type == 'monthly':
                        kewajiban_siswa_ids = siswa_id.kewajiban_ids.filtered(lambda x: x.biaya_id.id == self.biaya_id.id and x.month == self.month and x.year == self.year)
                    else:
                        kewajiban_siswa_ids = siswa_id.kewajiban_ids.filtered(lambda x: x.biaya_id.id == self.biaya_id.id)

                    if kewajiban_siswa_ids:
                        kewajiban_siswa_ids.write({
                            'jumlah': self.jumlah
                        })
        return True