# -*- coding: utf-8 -*-

from odoo import api, models, fields, _

import logging, datetime

_logger = logging.getLogger(__name__)

class LaporanInformasiSiswa(models.TransientModel):
    _name = 'efarina.laporan.informasisiswa'

    def export_xlsx(self):
        return self.env['efarina.siswa'].export_informasisiswa_xlsx()