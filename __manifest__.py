# -*- coding: utf-8 -*-
{
    'name' : 'Yayasan Efarina',
    'version' : '1.1',
    'summary': 'Management for Yayasan Efarina',
    'sequence': 10,
    'category': 'Education',
    'author': 'Daniel D Fortuna',
    'depends' : [
        'base_setup', 
        'mail', 
        'report_xlsx_helper', # From OCA/reporting-engine
        'web_notify', # From OCA/web
    ],
    'data': [
        'security/efarina_security.xml',
        'security/ir.model.access.csv',

        'data/ir_sequence_data.xml',

        # 'data/efarina_data.xml',
        # "data/efarina_demo_data.xml",
        # 'data/efarina_product_data.xml',
        
        'views/efarina_aset_views.xml',
        'views/efarina_biaya_views.xml',
        'views/efarina_asrama_views.xml',
        'views/efarina_jurusan_views.xml',
        'views/efarina_kelas_views.xml',
        'views/efarina_guru_views.xml',
        'views/efarina_siswa_views.xml',
        'views/efarina_semester_views.xml',
        'views/efarina_order_views.xml',
        'views/efarina_account_views.xml',
        'views/efarina_jenispendapatan_views.xml',
        'views/efarina_jenispengeluaran_views.xml',
        'views/efarina_tingkatan_views.xml',

        'views/efarina_report.xml',

        'wizard/update_biaya_kewajiban_views.xml',

        'views/efarina_menuitem.xml',

        'wizard/laporan_orderday_views.xml',
        'wizard/laporan_ordermonth_views.xml',
        'wizard/laporan_resume_views.xml',
        'wizard/laporan_piutangkelas_views.xml',
        'wizard/laporan_penagihan_views.xml',
        'wizard/laporan_informasisiswa_views.xml',
        'wizard/laporan_penagihansiswa_views.xml',
        'wizard/laporan_piutangbulanan_views.xml',
    ],
    'qweb':[
        "static/src/xml/base.xml",
    ],
    'installable': True,
    'application': True,
}
