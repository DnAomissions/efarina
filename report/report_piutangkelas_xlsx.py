# -*- coding: utf-8 -*-

import logging, datetime, xlsxwriter, calendar, pytz
from dateutil.relativedelta import relativedelta

from odoo import models
from odoo.exceptions import UserError

_logger = logging.getLogger(__name__)

class LaporanPiutangKelas(models.AbstractModel):
    _name = 'report.efarina.piutangkelas_xlsx'
    _inherit = 'report.report_xlsx.abstract'
    
    def generate_xlsx_report(self, workbook, data, order):
        kelas_ids = data['context'].get('kelas_ids')
        # _logger.info(kelas_id)
        start_year = int(data['context'].get('start_year'))
        start_month = int(data['context'].get('start_month'))
        end_year = int(data['context'].get('end_year'))
        end_month = int(data['context'].get('end_month'))

        date_start = datetime.date(start_year, start_month, 1)
        date_end = datetime.date(end_year, end_month, 1)
        
        # user_tz = pytz.timezone(data['context'].get('tz') or self.env.user.tz or pytz.utc)
        # start_utc_datetime = user_tz.localize(date_start).astimezone(pytz.utc)
        # end_utc_datetime = user_tz.localize(date_end).astimezone(pytz.utc)

        # Style
        bold = workbook.add_format({'bold': True})
        header = workbook.add_format({
            'bg_color': '#8EA9DB',
            'border': 1
        })
        border = workbook.add_format({
            'border': 1
        })
        money_format = workbook.add_format({'num_format': '"Rp." #,##0', 'border': 1})
        money_format_bold = workbook.add_format({'bold': True, 'num_format': '"Rp." #,##0', 'border': 1})
        date_format = workbook.add_format({'num_format': 'd mmmm yyyy', 'border': 1})

        # Variable
        bulan = [
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        ]

        bulan_s = [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'Mei',
            'Jun',
            'Jul',
            'Agu',
            'Sep',
            'Okt',
            'Nov',
            'Des'
        ]
        kelas = self.env['efarina.kelas'].browse(kelas_ids)
        for kelas_id in kelas:
            sheet = workbook.add_worksheet("{}".format(kelas_id.name))
            # Column Width Setup
            sheet.set_column(0, 1, 4) # A - B
            sheet.set_column(2, 2, 45) # C
            sheet.set_column(3, 3, 4) # D


            # Start Sheet
            col = 1
            row = 0
            sheet.merge_range('B1:K1', "REKPITULASI STATUS KEWAJIBAN SISWA KELAS {} SMA PLUS EFARINA".format(kelas_id.name), bold)
            row = row + 1
            sheet.merge_range('B2:K2', "Jl. Nawar Tager No.01,")
            row = row + 1
            sheet.merge_range('B3:K3', "Kelurahan Saribudolok, Kec. Silimakuta, Kab. Simalungun, Sumatera Utara")
            row = row + 1

            # Header
            col = 1
            row = row + 1
            sheet.write(row, col, "No", header)
            col = col + 1
            sheet.write(row, col, "Nama", header)
            col = col + 1

            for kewajiban in self.env['efarina.kelas.kewajiban'].search([('kelas_id', '=', kelas_id.id), ('type', 'not in', ['monthly'])]):
                sheet.set_column(col, col, 15)
                sheet.write(row, col, kewajiban.name, header)
                col = col + 1

            # Bulan Sebelumnya
            sheet.set_column(col, col, 15)
            sheet.write(row, col, "Bulan Sebelumnya", header)
            col = col + 1

            date_start = datetime.date(start_year, start_month, 1)
            while date_start <= date_end:
                sheet.set_column(col, col, 15)
                sheet.write(row, col, "{} {}".format(bulan_s[date_start.month-1], date_start.year), header)
                date_start += relativedelta(months=1)
                col = col + 1

            sheet.set_column(col, col, 15)
            sheet.write(row, col, "Total", header)
            sheet.write(row-1, col-1, "Wali Kelas", bold)
            sheet.write(row-1, col, kelas_id.guru_id.name if kelas_id.guru_id else '', bold)
            col = col + 1
            sheet.set_column(col, col, 30)
            sheet.write(row, col, "Catatan", header)
            col = col + 1

            no = 1
            row = row + 1
            col = 1
            first_row = row
            for siswa in kelas_id.siswa_ids:
                sheet.write(row, col, no, border)
                col = col + 1
                sheet.write(row, col, siswa.name, border)
                col = col + 1
                # Kewajiban
                for kewajiban in self.env['efarina.kelas.kewajiban'].search([('kelas_id', '=', kelas_id.id), ('type', 'not in', ['monthly'])]):
                    sheet.write(row, col, sum(kwj.sisa_harus_bayar for kwj in self.env['efarina.siswa.kewajiban'].search([('siswa_id', '=', siswa.id), ('biaya_id', '=', kewajiban.biaya_id.id)])), money_format)
                    col = col + 1
                
                
                # Bulanan
                date_start = datetime.date(start_year, start_month, 1)

                # Bulan Sebelumnya
                sheet.write(row, col, sum(kwj.sisa_harus_bayar for kwj in self.env['efarina.siswa.kewajiban'].search([('siswa_id', '=', siswa.id), ('type', '=', 'monthly'), ('date_month_year', '<', date_start)])), money_format)
                col = col + 1
                while date_start <= date_end:
                    sheet.write(row, col, sum(kwj.sisa_harus_bayar for kwj in self.env['efarina.siswa.kewajiban'].search([('siswa_id', '=', siswa.id), ('type', '=', 'monthly'), ('year', '=', date_start.year), ('month', '=', str(date_start.month))])), money_format)
                    date_start += relativedelta(months=1)
                    col = col + 1

                sheet.write(row, col, '=SUM(C{}:{}{})'.format(row+1, xlsxwriter.utility.xl_col_to_name(col-1), row+1), money_format)
                col = col + 1
                sheet.write(row, col, '', border)
                row = row + 1
                col = 1
                no = no + 1
            
            # Total
            # sheet.write(row, col, no)
            col = col + 1
            sheet.merge_range('B{}:C{}'.format(row+1, row+1), 'JUMLAH TOTAL', border)
            # sheet.write(row, col, 'JUMLAH TOTAL', border)
            col = col + 1
            # Kewajiban
            for kewajiban in self.env['efarina.kelas.kewajiban'].search([('kelas_id', '=', kelas_id.id), ('type', 'not in', ['monthly'])]):
                sheet.write(row, col, '=SUM({}{}:{}{})'.format(xlsxwriter.utility.xl_col_to_name(col), first_row+1, xlsxwriter.utility.xl_col_to_name(col), row), money_format)
                col = col + 1
            # Bulanan
            date_start = datetime.date(start_year, start_month, 1)

            # Bulan sebelumnya
            sheet.write(row, col, '=SUM({}{}:{}{})'.format(xlsxwriter.utility.xl_col_to_name(col), first_row+1, xlsxwriter.utility.xl_col_to_name(col), row), money_format)
            col = col + 1

            while date_start <= date_end:
                sheet.write(row, col, '=SUM({}{}:{}{})'.format(xlsxwriter.utility.xl_col_to_name(col), first_row+1, xlsxwriter.utility.xl_col_to_name(col), row), money_format)
                date_start += relativedelta(months=1)
                col = col + 1
            sheet.write(row, col, '=SUM({}{}:{}{})'.format(xlsxwriter.utility.xl_col_to_name(col), first_row+1, xlsxwriter.utility.xl_col_to_name(col), row), money_format)
            col = col + 1
            sheet.write(row, col, '', border)
            # sheet.write(row, col, '=SUM(C{}:{}{})'.format(row+1, xlsxwriter.utility.xl_col_to_name(col-1), row+1), money_format)
            # End Sheet