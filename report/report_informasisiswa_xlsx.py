# -*- coding: utf-8 -*-

import logging, datetime, xlsxwriter, calendar, pytz
from dateutil.relativedelta import relativedelta

from odoo import models
from odoo.exceptions import UserError

_logger = logging.getLogger(__name__)

class LaporanInformasiSiswa(models.AbstractModel):
    _name = 'report.efarina.informasisiswa_xlsx'
    _inherit = 'report.report_xlsx.abstract'
    
    def generate_xlsx_report(self, workbook, data, order):

        # Style
        bold = workbook.add_format({'bold': True})
        money_format = workbook.add_format({'num_format': '"Rp." #,##0', 'border': 1})
        money_format_bold = workbook.add_format({'bold': True, 'num_format': '"Rp." #,##0', 'border': 1})
        date_format = workbook.add_format({'num_format': 'd mmmm yyyy', 'border': 1})
        border = workbook.add_format({'border': 1})
        bold_border = workbook.add_format({'bold': True, 'border': 1})

        # Variable
        bulan = [
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        ]

        tingkatan_ids = self.env['efarina.tingkatan'].search([('active', '=', True), ('tingkat', '!=', 'alumni')])

        # Informasi Siswa
        sheet = workbook.add_worksheet("Informasi Siswa")
        
        # Column Width Setup
        sheet.set_column(0, 0, 2) # A
        sheet.set_column(1, 1, 20) # B
        sheet.set_column(2, 4, 10) # C - E

        col = 1
        row = 0

        sheet.write(row, col, "REKAPITULASI SISWA/I SMA/SMK PLUS EFARINA", bold)
        row += 1

        # SMA
        sheet.write(row, col, "SMA", bold)
        row += 1
        sheet.write(row, col, "TINGKAT", bold_border)
        sheet.write(row, col+1, "L", bold_border)
        sheet.write(row, col+2, "P", bold_border)
        sheet.write(row, col+3, "JUMLAH", bold_border)
        
        row += 1
        first_row = row
        sheet.write(row, col, "X", border)
        sheet.write(row, col+1, len(self.env['efarina.siswa'].search([('tingkatan_id', 'in', tingkatan_ids.ids), ('tingkat', '=', 'x'), ('pendidikan', '=', 'SMA'), ('jenis_kelamin', '=', 'male')])), border)
        sheet.write(row, col+2, len(self.env['efarina.siswa'].search([('tingkatan_id', 'in', tingkatan_ids.ids), ('tingkat', '=', 'x'), ('pendidikan', '=', 'SMA'), ('jenis_kelamin', '=', 'female')])), border)
        sheet.write(row, col+3, "=SUM(C{}:D{})".format(row+1, row+1), bold_border)

        row += 1
        sheet.write(row, col, "XI", border)
        sheet.write(row, col+1, len(self.env['efarina.siswa'].search([('tingkatan_id', 'in', tingkatan_ids.ids), ('tingkat', '=', 'xi'), ('pendidikan', '=', 'SMA'), ('jenis_kelamin', '=', 'male')])), border)
        sheet.write(row, col+2, len(self.env['efarina.siswa'].search([('tingkatan_id', 'in', tingkatan_ids.ids), ('tingkat', '=', 'xi'), ('pendidikan', '=', 'SMA'), ('jenis_kelamin', '=', 'female')])), border)
        sheet.write(row, col+3, "=SUM(C{}:D{})".format(row+1, row+1), bold_border)

        row += 1
        sheet.write(row, col, "XII", border)
        sheet.write(row, col+1, len(self.env['efarina.siswa'].search([('tingkatan_id', 'in', tingkatan_ids.ids), ('tingkat', '=', 'xii'), ('pendidikan', '=', 'SMA'), ('jenis_kelamin', '=', 'male')])), border)
        sheet.write(row, col+2, len(self.env['efarina.siswa'].search([('tingkatan_id', 'in', tingkatan_ids.ids), ('tingkat', '=', 'xii'), ('pendidikan', '=', 'SMA'), ('jenis_kelamin', '=', 'female')])), border)
        sheet.write(row, col+3, "=SUM(C{}:D{})".format(row+1, row+1), bold_border)

        row += 1
        sheet.write(row, col, "TOTAL", bold_border)
        sheet.write(row, col+1, "=SUM(C{}:C{})".format(first_row+1, row), bold_border)
        sheet.write(row, col+2, "=SUM(D{}:D{})".format(first_row+1, row), bold_border)
        sheet.write(row, col+3, "=SUM(E{}:E{})".format(first_row+1, row), bold_border)

        row += 2

        # SMK
        sheet.write(row, col, "SMK", bold)
        row += 1
        sheet.write(row, col, "TINGKAT", bold_border)
        sheet.write(row, col+1, "L", bold_border)
        sheet.write(row, col+2, "P", bold_border)
        sheet.write(row, col+3, "JUMLAH", bold_border)
        
        row += 1
        first_row = row
        sheet.write(row, col, "X", border)
        sheet.write(row, col+1, len(self.env['efarina.siswa'].search([('tingkatan_id', 'in', tingkatan_ids.ids), ('tingkat', '=', 'x'), ('pendidikan', '=', 'SMK'), ('jenis_kelamin', '=', 'male')])), border)
        sheet.write(row, col+2, len(self.env['efarina.siswa'].search([('tingkatan_id', 'in', tingkatan_ids.ids), ('tingkat', '=', 'x'), ('pendidikan', '=', 'SMK'), ('jenis_kelamin', '=', 'female')])), border)
        sheet.write(row, col+3, "=SUM(C{}:D{})".format(row+1, row+1), bold_border)

        row += 1
        sheet.write(row, col, "XI", border)
        sheet.write(row, col+1, len(self.env['efarina.siswa'].search([('tingkatan_id', 'in', tingkatan_ids.ids), ('tingkat', '=', 'xi'), ('pendidikan', '=', 'SMK'), ('jenis_kelamin', '=', 'male')])), border)
        sheet.write(row, col+2, len(self.env['efarina.siswa'].search([('tingkatan_id', 'in', tingkatan_ids.ids), ('tingkat', '=', 'xi'), ('pendidikan', '=', 'SMK'), ('jenis_kelamin', '=', 'female')])), border)
        sheet.write(row, col+3, "=SUM(C{}:D{})".format(row+1, row+1), bold_border)

        row += 1
        sheet.write(row, col, "XII", border)
        sheet.write(row, col+1, len(self.env['efarina.siswa'].search([('tingkatan_id', 'in', tingkatan_ids.ids), ('tingkat', '=', 'xii'), ('pendidikan', '=', 'SMK'), ('jenis_kelamin', '=', 'male')])), border)
        sheet.write(row, col+2, len(self.env['efarina.siswa'].search([('tingkatan_id', 'in', tingkatan_ids.ids), ('tingkat', '=', 'xii'), ('pendidikan', '=', 'SMK'), ('jenis_kelamin', '=', 'female')])), border)
        sheet.write(row, col+3, "=SUM(C{}:D{})".format(row+1, row+1), bold_border)

        row += 1
        sheet.write(row, col, "TOTAL", bold_border)
        sheet.write(row, col+1, "=SUM(C{}:C{})".format(first_row+1, row), bold_border)
        sheet.write(row, col+2, "=SUM(D{}:D{})".format(first_row+1, row), bold_border)
        sheet.write(row, col+3, "=SUM(E{}:E{})".format(first_row+1, row), bold_border)

        row += 2

        # SMA & SMK
        sheet.write(row, col, "SMA & SMK", bold)
        row += 1
        sheet.write(row, col, "TINGKAT", bold_border)
        sheet.write(row, col+1, "L", bold_border)
        sheet.write(row, col+2, "P", bold_border)
        sheet.write(row, col+3, "JUMLAH", bold_border)
        
        row += 1
        first_row = row
        sheet.write(row, col, "X", border)
        sheet.write(row, col+1, len(self.env['efarina.siswa'].search([('tingkatan_id', 'in', tingkatan_ids.ids), ('tingkat', '=', 'x'), ('pendidikan', 'in', ['SMA', 'SMK']), ('jenis_kelamin', '=', 'male')])), border)
        sheet.write(row, col+2, len(self.env['efarina.siswa'].search([('tingkatan_id', 'in', tingkatan_ids.ids), ('tingkat', '=', 'x'), ('pendidikan', 'in', ['SMA', 'SMK']), ('jenis_kelamin', '=', 'female')])), border)
        sheet.write(row, col+3, "=SUM(C{}:D{})".format(row+1, row+1), bold_border)

        row += 1
        sheet.write(row, col, "XI", border)
        sheet.write(row, col+1, len(self.env['efarina.siswa'].search([('tingkatan_id', 'in', tingkatan_ids.ids), ('tingkat', '=', 'xi'), ('pendidikan', 'in', ['SMA', 'SMK']), ('jenis_kelamin', '=', 'male')])), border)
        sheet.write(row, col+2, len(self.env['efarina.siswa'].search([('tingkatan_id', 'in', tingkatan_ids.ids), ('tingkat', '=', 'xi'), ('pendidikan', 'in', ['SMA', 'SMK']), ('jenis_kelamin', '=', 'female')])), border)
        sheet.write(row, col+3, "=SUM(C{}:D{})".format(row+1, row+1), bold_border)

        row += 1
        sheet.write(row, col, "XII", border)
        sheet.write(row, col+1, len(self.env['efarina.siswa'].search([('tingkatan_id', 'in', tingkatan_ids.ids), ('tingkat', '=', 'xii'), ('pendidikan', 'in', ['SMA', 'SMK']), ('jenis_kelamin', '=', 'male')])), border)
        sheet.write(row, col+2, len(self.env['efarina.siswa'].search([('tingkatan_id', 'in', tingkatan_ids.ids), ('tingkat', '=', 'xii'), ('pendidikan', 'in', ['SMA', 'SMK']), ('jenis_kelamin', '=', 'female')])), border)
        sheet.write(row, col+3, "=SUM(C{}:D{})".format(row+1, row+1), bold_border)
        
        row += 1
        sheet.write(row, col, "TOTAL", bold_border)
        sheet.write(row, col+1, "=SUM(C{}:C{})".format(first_row+1, row), bold_border)
        sheet.write(row, col+2, "=SUM(D{}:D{})".format(first_row+1, row), bold_border)
        sheet.write(row, col+3, "=SUM(E{}:E{})".format(first_row+1, row), bold_border)

        # Asrama
        sheet = workbook.add_worksheet("Asrama")
        
        # Column Width Setup
        sheet.set_column(0, 0, 2) # A
        sheet.set_column(1, 1, 20) # B
        sheet.set_column(2, 4, 10) # C - E

        col = 1
        row = 0

        sheet.write(row, col, "REKAPITULASI SISWA/I SMA/SMK PLUS EFARINA", bold)
        row += 2

        sheet.write(row, col, "ASRAMA", bold_border)
        sheet.write(row, col+1, "L", bold_border)
        sheet.write(row, col+2, "P", bold_border)
        sheet.write(row, col+3, "JUMLAH", bold_border)

        row += 1
        first_row = row
        for asrama_id in self.env['efarina.asrama'].search([]):
            sheet.write(row, col, asrama_id.name, border)
            sheet.write(row, col+1, len(self.env['efarina.siswa'].search([('tingkatan_id', 'in', tingkatan_ids.ids), ('asrama_id', '=', asrama_id.id), ('jenis_kelamin', '=', 'male')])), border)
            sheet.write(row, col+2, len(self.env['efarina.siswa'].search([('tingkatan_id', 'in', tingkatan_ids.ids), ('asrama_id', '=', asrama_id.id), ('jenis_kelamin', '=', 'female')])), border)
            sheet.write(row, col+3, "=SUM(C{}:D{})".format(row+1, row+1), bold_border)
            row += 1

        sheet.write(row, col, "TOTAL", bold_border)
        sheet.write(row, col+1, "=SUM(C{}:C{})".format(first_row+1, row), bold_border)
        sheet.write(row, col+2, "=SUM(D{}:D{})".format(first_row+1, row), bold_border)
        sheet.write(row, col+3, "=SUM(E{}:E{})".format(first_row+1, row), bold_border)