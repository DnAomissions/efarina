# -*- coding: utf-8 -*-

from odoo import api, models, fields, _

import logging

_logger = logging.getLogger(__name__)

class Asrama(models.Model):
    _name = 'efarina.asrama'

    name = fields.Char('Nama Asrama', required=True)

    siswa_ids = fields.One2many('efarina.siswa', 'asrama_id', 'Siswa')
    siswa_count = fields.Integer('# Siswa',
        compute='_compute_siswa_count', compute_sudo=False)

    def _compute_siswa_count(self):
        for asrama in self:
            asrama.siswa_count = self.env['efarina.siswa'].search_count([('asrama_id', '=', asrama.id)])

    def action_view_siswa(self):
        self.ensure_one()
        action = self.env["ir.actions.actions"]._for_xml_id('efarina.action_efarina_siswa')
        siswa = self.mapped('siswa_ids')

        if len(siswa) > 1:
            action['domain'] = [('id', 'in', siswa.ids)]

        elif siswa:
            form_view = [(self.env.ref('efarina.view_efarina_siswa_form').id, 'form')]
            if 'views' in action:
                action['views'] = form_view + [
                    (state, view) for state, view in action['views']
                    if view != 'form']

            else:
                action['views'] = form_view

            action['res_id'] = siswa.id

        action['context'] = dict(self._context, default_origin=self.name,
                                 create=False)

        return action