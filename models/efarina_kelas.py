# -*- coding: utf-8 -*-

from odoo import api, models, fields, _
from odoo.exceptions import UserError

import logging, datetime

_logger = logging.getLogger(__name__)

class Kelas(models.Model):
    _name = 'efarina.kelas'

    name = fields.Char('Nama Kelas', required=True, index=True)
    active = fields.Boolean('Active', default=True, help="If unchecked, it will allow you to hide the kelas without removing it.")
    siswa_ids = fields.One2many('efarina.siswa', 'kelas_id', 'Siswa')
    siswa_count = fields.Integer('# Siswa',
        compute='_compute_siswa_count', compute_sudo=False)

    def _compute_siswa_count(self):
        for kelas in self:
            kelas.siswa_count = self.env['efarina.siswa'].search_count([('kelas_id', '=', kelas.id)])

    jurusan_id = fields.Many2one('efarina.jurusan', 'Jurusan', required=True)
    tingkatan_id = fields.Many2one('efarina.tingkatan', 'Tingkatan', required=True)
    guru_id = fields.Many2one('efarina.guru', 'Wali Kelas')

    pendidikan = fields.Selection(string='Pendidikan', related='tingkatan_id.pendidikan', store=True)
    tingkat = fields.Selection(string="Tingkat", related='tingkatan_id.tingkat', store=True)
    kewajiban_ids = fields.One2many('efarina.kelas.kewajiban', 'kelas_id', 'Biaya')
    kewajiban_checked_all = fields.Boolean('Checked All?', default=False, compute='_compute_checked_all')

    @api.depends('kewajiban_ids', 'kewajiban_ids.publish_data')
    def _compute_checked_all(self):
        for kelas in self:
            kelas.kewajiban_checked_all = all(kelas.kewajiban_ids.mapped('publish_data'))
            
    def get_default_kewajiban(self):
        if not self.tingkatan_id:
            raise UserError(_("Harap Pilih Kelas terlebih dahulu!"))
        else:
            for kewajiban in self.tingkatan_id.kewajiban_ids.filtered(lambda x: x.publish_data == True):
                exists = False
                domain = [
                    ('kelas_id', '=', self.id),
                    ('biaya_id', '=', kewajiban.biaya_id.id),
                ]

                if kewajiban.type == 'annualy':
                    domain.append(('year', '=', str(datetime.datetime.today().year)))

                kewajiban_kelas = self.env['efarina.kelas.kewajiban'].search(domain)
                if len(kewajiban_kelas) > 0:
                    exists = True
                        
                if kewajiban.type in ['monthly']:
                    exists = False

                if not exists:
                    if kewajiban.biaya_id.type == 'monthly':
                        # Ini nanti harus menyesuaikan
                        bulans = [
                            'Januari',
                            'Februari',
                            'Maret',
                            'April',
                            'Mei',
                            'Juni',
                            'Juli',
                            'Agustus',
                            'September',
                            'Oktober',
                            'November',
                            'Desember'
                        ]
                        for bulan in range(1,13):
                            existsDetail = False
                            domain = [
                                ('kelas_id', '=', self.id),
                                ('biaya_id', '=', kewajiban.biaya_id.id),
                                ('month', '=', str(bulan)),
                                ('year', '=', str(datetime.datetime.today().year)),
                            ]
                            kewajiban_kelas = self.env['efarina.kelas.kewajiban'].search(domain)
                            if len(kewajiban_kelas) > 0:
                                existsDetail = True
                            
                            if not existsDetail:
                                self.env['efarina.kelas.kewajiban'].create({
                                    'kelas_id': self.id,
                                    'name': kewajiban.name,
                                    'biaya_id': kewajiban.biaya_id.id,
                                    'jumlah': kewajiban.jumlah,
                                    'currency_id': kewajiban.currency_id.id,
                                    'type': kewajiban.type,
                                    'month': str(bulan),
                                    'year': str(datetime.datetime.today().year)
                                })
                            else:
                                self.env.user.notify_info(message=_("%s sudah memiliki kewajiban %s") % (self.name, "{} ({} {})".format(kewajiban.name, bulans[bulan-1], str(datetime.datetime.today().year))))
                                _logger.info(_("%s sudah memiliki kewajiban %s") % (self.name, "{} ({} {})".format(kewajiban.name, bulans[bulan-1], str(datetime.datetime.today().year))))
                    else:
                        self.env['efarina.kelas.kewajiban'].create({
                            'kelas_id': self.id,
                            'name': kewajiban.name,
                            'biaya_id': kewajiban.biaya_id.id,
                            'jumlah': kewajiban.jumlah,
                            'currency_id': kewajiban.currency_id.id,
                            'type': kewajiban.type,
                        })
                else:
                    self.env.user.notify_info(message=_("%s sudah memiliki kewajiban %s") % (self.name, kewajiban.name))
                    _logger.info(_("%s sudah memiliki kewajiban %s") % (self.name, kewajiban.name))
        return True

    kewajiban_count = fields.Integer('# Kewajiban', compute='_compute_kewajiban_count')

    _sql_constraints = [
        ('name_uniq', 'unique (name)', 'The name of the kelas must be unique !')
    ]

    def _compute_kewajiban_count(self):
        for kelas in self:
            kelas.kewajiban_count = self.env['efarina.kelas.kewajiban'].search_count([('kelas_id', '=', kelas.id)])

    def assign_all_kewajiban(self):
        if not self.kewajiban_ids:
            raise UserError(_("Kewajiban Masih Kosong!"))
        self.check_all_publish()
        self.assign_kewajiban()
        return True

    def check_all_publish(self):
        self.env['efarina.kelas.kewajiban'].search([('kelas_id', '=', self.id)]).write({
            'publish_data': True
        })
        return True
    
    def uncheck_all_publish(self):
        self.env['efarina.kelas.kewajiban'].search([('kelas_id', '=', self.id)]).write({
            'publish_data': False
        })
        return True
        
    def assign_kewajiban(self):
        if not self.siswa_ids:
            raise UserError(_("Harap Pilih Kelas terlebih dahulu!"))
        else:
            for siswa in self.siswa_ids:
                siswa.get_default_kewajiban()

        self.env['efarina.kelas.kewajiban'].search([('kelas_id', '=', self.id)]).write({
            'publish_data': False
        })
        return True

    def action_view_siswa(self):
        self.ensure_one()
        action = self.env["ir.actions.actions"]._for_xml_id('efarina.action_efarina_siswa')
        siswa = self.mapped('siswa_ids')

        if len(siswa) > 1:
            action['domain'] = [('id', 'in', siswa.ids)]

        elif siswa:
            form_view = [(self.env.ref('efarina.view_efarina_siswa_form').id, 'form')]
            if 'views' in action:
                action['views'] = form_view + [
                    (state, view) for state, view in action['views']
                    if view != 'form']

            else:
                action['views'] = form_view

            action['res_id'] = siswa.id

        action['context'] = dict(self._context, default_origin=self.name,
                                 create=False)

        return action

class Kewajiban(models.Model):
    _name = 'efarina.kelas.kewajiban'

    def _get_default_currency_id(self):
        return self.env.company.currency_id.id

    publish_data = fields.Boolean('Publish?', required=True, default=False)
    kelas_id = fields.Many2one('efarina.kelas', 'Kelas', ondelete="cascade")
    name = fields.Char('Nama Kewajiban', required=True)
    biaya_id = fields.Many2one('efarina.biaya', 'Biaya', required=True)
    jumlah = fields.Monetary('Jumlah', default=0)
    currency_id = fields.Many2one('res.currency', 'Currency', default=_get_default_currency_id, required=True)
    type = fields.Selection(string="Jenis", related="biaya_id.type")
    due_date = fields.Date('Batas Tanggal Terakhir')

    @api.model
    def year_selection(self):
        year = 2015 # replace 2015 with your a start year
        year_list = []
        while year != 2035: # replace 2035 with your end year
            year_list.append((str(year), str(year)))
            year += 1
        return year_list

    year = fields.Selection(
        year_selection,
        string="Year",
        default=str(datetime.datetime.today().year), # as a default value it would be 2021
    )

    month = fields.Selection([
        ('1', 'January'), 
        ('2', 'February'), 
        ('3', 'March'), 
        ('4', 'April'),
        ('5', 'May'), 
        ('6', 'June'), 
        ('7', 'July'), 
        ('8', 'August'), 
        ('9', 'September'), 
        ('10', 'October'), 
        ('11', 'November'), 
        ('12', 'December')
    ], 'Bulan', default=str(datetime.datetime.today().month))

    @api.onchange('biaya_id')
    def _onchange_biaya(self):
        if self.biaya_id:
            self.name = self.biaya_id.name
            self.jumlah = self.biaya_id.jumlah