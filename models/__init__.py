# -*- coding: utf-8 -*-

from . import efarina_aset
from . import efarina_asrama
from . import efarina_biaya
from . import efarina_guru
from . import efarina_jurusan
from . import efarina_kelas
from . import efarina_semester
from . import efarina_siswa
from . import efarina_order
from . import efarina_jenispendapatan
from . import efarina_tingkatan
from . import efarina_account
from . import efarina_jenispengeluaran