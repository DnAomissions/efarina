# -*- coding: utf-8 -*-

from odoo import api, models, fields, _

import logging

_logger = logging.getLogger(__name__)

class Aset(models.Model):
    _name = 'efarina.aset'

    def _get_default_currency_id(self):
        return self.env.company.currency_id.id
        
    name = fields.Char('Nama Bank / Cash', required=True)

    saldo = fields.Monetary('Saldo', compute="_compute_order", store=True)
    account_id = fields.Many2one('efarina.account', 'Akun Bank or Cash')
    currency_id = fields.Many2one('res.currency', 'Currency', default=_get_default_currency_id, required=True)

    order_ids = fields.One2many('efarina.order', 'aset_id', 'Orders')

    in_order_ids = fields.One2many('efarina.order', 'aset_id', compute='_compute_order')
    out_order_ids = fields.One2many('efarina.order', 'aset_id', compute='_compute_order')

    count_order_in = fields.Integer('# Order In', compute="_compute_order", store=True)
    count_order_out = fields.Integer('# Order Out', compute="_compute_order", store=True)
    
    total_order_in = fields.Monetary('Total Income', compute="_compute_order")
    total_order_out = fields.Monetary('Total Outcome', compute="_compute_order")

    _sql_constraints = [
        ('name_uniq', 'unique (name)', 'The name of the Bank or Cash must be unique !')
    ]

    @api.depends('order_ids', 'order_ids.state', 'order_ids.type', 'order_ids.jumlah')
    def _compute_order(self):
        for aset in self:
            aset.in_order_ids = aset.order_ids.filtered(lambda x: x.type == 'in')
            aset.out_order_ids = aset.order_ids.filtered(lambda x: x.type == 'out')

            aset.count_order_in = len(aset.in_order_ids)
            aset.count_order_out = len(aset.out_order_ids)

            aset.total_order_in = sum(aset.in_order_ids.filtered(lambda x: x.state in ['confirmed', 'done']).mapped('jumlah'))
            aset.total_order_out = sum(aset.out_order_ids.filtered(lambda x: x.state in ['confirmed', 'done']).mapped('jumlah'))

            aset.saldo = aset.total_order_in - aset.total_order_out

    def action_view_order_in(self):
        self.ensure_one()
        action = self.env["ir.actions.actions"]._for_xml_id('efarina.action_efarina_order_in')
        order = self.mapped('in_order_ids')

        if len(order) > 1:
            action['domain'] = [('id', 'in', order.ids)]

        elif order:
            form_view = [(self.env.ref('efarina.view_efarina_order_form').id, 'form')]
            if 'views' in action:
                action['views'] = form_view + [
                    (state, view) for state, view in action['views']
                    if view != 'form']

            else:
                action['views'] = form_view

            action['res_id'] = order.id

        action['context'] = dict(self._context, default_origin=self.name,
                                 create=False)

        return action

    def action_view_order_out(self):
        self.ensure_one()
        action = self.env["ir.actions.actions"]._for_xml_id('efarina.action_efarina_order_out')
        order = self.mapped('out_order_ids')

        if len(order) > 1:
            action['domain'] = [('id', 'in', order.ids)]

        elif order:
            form_view = [(self.env.ref('efarina.view_efarina_order_form').id, 'form')]
            if 'views' in action:
                action['views'] = form_view + [
                    (state, view) for state, view in action['views']
                    if view != 'form']

            else:
                action['views'] = form_view

            action['res_id'] = order.id

        action['context'] = dict(self._context, default_origin=self.name,
                                 create=False)

        return action