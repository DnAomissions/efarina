# -*- coding: utf-8 -*-

from odoo import api, models, fields, _

import logging

_logger = logging.getLogger(__name__)

class Semester(models.Model):
    _name = 'efarina.semester'

    name = fields.Char('Nama Semester', required=True)
    @api.model
    def year_selection(self):
        year = 2015 # replace 2015 with your a start year
        year_list = []
        while year != 2035: # replace 2035 with your end year
            year_list.append((str(year), str(year)))
            year += 1
        return year_list

    year = fields.Selection(
        year_selection,
        string="Year",
        default="2021", # as a default value it would be 2021
    )

    bulan_ids = fields.One2many('efarina.semester.bulan', 'semester_id', 'Bulan')

class SemesterLine(models.Model):
    _name = 'efarina.semester.bulan'

    semester_id = fields.Many2one('efarina.semester', 'Semester')
    month = fields.Selection([
        ('1', 'January'), 
        ('2', 'February'), 
        ('3', 'March'), 
        ('4', 'April'),
        ('5', 'May'), 
        ('6', 'June'), 
        ('7', 'July'), 
        ('8', 'August'), 
        ('9', 'September'), 
        ('10', 'October'), 
        ('11', 'November'), 
        ('12', 'December')
    ], 'Bulan', required=True)