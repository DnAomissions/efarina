# -*- coding: utf-8 -*-

from odoo import api, models, fields, _

import logging

_logger = logging.getLogger(__name__)

class Guru(models.Model):
    _name = 'efarina.guru'

    name = fields.Char('Nama Guru', required=True)
    kelas_id = fields.Many2one('efarina.kelas', 'Kelas', compute="_compute_kelas_id")
    kelas_ids = fields.One2many('efarina.kelas', 'guru_id', 'Kelas')

    @api.depends('kelas_ids')
    def _compute_kelas_id(self):
        for guru in self:
            if len(guru.kelas_ids) > 0:
                guru.kelas_id = guru.kelas_ids[0]
                guru.status = 'wali_kelas'
            else:
                guru.kelas_id = False
                guru.status = 'guru'

    pendidikan = fields.Selection([
        ('SMA', 'SMA'),
        ('SMK', 'SMK')
    ], 'Pendidikan')
    jurusan_id = fields.Many2one('efarina.jurusan', 'Jurusan')
    alamat = fields.Text('Alamat')
    agama = fields.Selection([
        ('islam', 'ISLAM'),
        ('protestan', 'PROTESTAN'),
        ('katolik', 'KATOLIK'),
        ('budha', 'BUDHA'),
        ('hindu', 'HINDU')
    ], 'Agama')
    tanggal_lahir = fields.Date('Tanggal Lahir')
    status = fields.Selection([
        ('guru', 'Guru'),
        ('wali_kelas', 'Wali Kelas')
    ], 'Status', default='guru', store=True, compute="_compute_kelas_id")
    no_hp = fields.Char('No Handphone')
    no_rek = fields.Char('No Rekening')