# -*- coding: utf-8 -*-

from odoo import api, models, fields, _

import logging

_logger = logging.getLogger(__name__)

class Jurusan(models.Model):
    _name = 'efarina.jurusan'

    name = fields.Char('Jurusan', required=True)
    kelas_ids = fields.One2many('efarina.kelas', 'jurusan_id', 'Kelas')

    _sql_constraints = [
        ('name_uniq', 'unique (name)', 'The name of the jurusan must be unique !')
    ]