## 1. Update Module
1. Akses SSH > `ssh admin@103.151.226.4`
```
> ssh admin@103.151.226.4
admin@103.151.226.4's password:
Last login: Fri Jul 30 22:26:32 2021 from 180.251.131.22
[admin@media-stream ~]$
```
2. Login sebagai *root* > `sudo su`
```
[admin@media-stream ~]$ sudo su
[sudo] password for admin:
[root@media-stream admin]#
```
3. Login sebagai *odoo* > `sudo su odoo`
```
[root@media-stream admin]# sudo su odoo
[odoo@media-stream admin]$
``` 
4. Masuk Home path dari user *odoo* > `cd ~/`
```
[odoo@media-stream admin]$ cd ~/
[odoo@media-stream ~]$
```
5. Eksekusi bash script untuk update module & restart service > `./update_module`
```
[odoo@media-stream ~]$ ./update_module
Already up-to-date.
2021-07-30 16:17:43,037 12149 INFO ? odoo.addons.bus.models.bus: Bus.loop listen imbus on db postgres
2021-07-30 16:17:48,049 12149 WARNING efarina odoo.modules.registry: efarina.aset: inconsistent 'compute_sudo' for computed fields: saldo, in_order_ids, out_order_ids, count_order_in, count_order_out, total_order_in, total_order_out
2021-07-30 16:17:48,049 12149 WARNING efarina odoo.modules.registry: efarina.guru: inconsistent 'compute_sudo' for computed fields: kelas_id, status
2021-07-30 16:17:48,060 12149 INFO efarina werkzeug: 180.251.131.22 - - [30/Jul/2021 16:17:48] "POST /longpolling/im_status HTTP/1.1" 200 - 17 0.011 0.017
2021-07-30 16:18:33,039 12149 INFO efarina werkzeug: 180.251.131.22 - - [30/Jul/2021 16:18:33] "POST /longpolling/poll HTTP/1.1" 200 - 346 0.063 51.470
2021-07-30 16:18:38,855 12149 INFO efarina werkzeug: 180.251.131.22 - - [30/Jul/2021 16:18:38] "POST /longpolling/im_status HTTP/1.1" 200 - 4 0.001 0.005
2021-07-30 16:19:23,125 12149 INFO efarina werkzeug: 180.251.131.22 - - [30/Jul/2021 16:19:23] "POST /longpolling/poll HTTP/1.1" 200 - 8 0.002 50.011
2021-07-30 16:19:29,836 12149 INFO efarina werkzeug: 180.251.131.22 - - [30/Jul/2021 16:19:29] "POST /longpolling/im_status HTTP/1.1" 200 - 4 0.001 0.005
2021-07-30 16:20:04,627 12149 INFO ? odoo.service.server: Initiating shutdown
2021-07-30 16:20:04,627 12149 INFO ? odoo.service.server: Hit CTRL-C again or send a second signal to force the shutdown.
2021-07-30 16:21:26,970 12345 INFO ? odoo: Odoo version 14.0+e-20210713
2021-07-30 16:21:26,970 12345 INFO ? odoo: Using configuration file at /etc/odoo.conf
2021-07-30 16:21:26,971 12345 INFO ? odoo: addons paths: ['/opt/odoo/odoo/addons', '/home/odoo/.local/share/Odoo/addons/14.0', '/home/odoo/addons', '/home/odoo/addons/reporting-engine', '/home/odoo/addons/web']
2021-07-30 16:21:26,971 12345 INFO ? odoo: database: default@default:default
2021-07-30 16:21:27,157 12345 INFO ? odoo.addons.base.models.ir_actions_report: You need Wkhtmltopdf to print a pdf version of the reports.
2021-07-30 16:21:27,251 12345 INFO ? odoo.service.server: HTTP service (werkzeug) running on media-stream:8069
```