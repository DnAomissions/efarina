## 1. Akses SSH:
```
User        : admin
Password    : admin2022
```

## 2. Directory Odoo & Module
```
Odoo    : /opt/odoo/
Module  : /home/odoo/addons/efarina
```

## 3. Akses Database PostgreSQL
```
Database    : efarina
User        : odoo
```
1. Akses SSH > `ssh admin@103.151.226.4`
```
> ssh admin@103.151.226.4
admin@103.151.226.4's password:
Last login: Fri Jul 30 22:26:32 2021 from 180.251.131.22
[admin@media-stream ~]$
```
2. Login sebagai *root* > `sudo su`
```
[admin@media-stream ~]$ sudo su
[sudo] password for admin:
[root@media-stream admin]#
```
3. Login sebagai *odoo* > `sudo su odoo`
```
[root@media-stream admin]# sudo su odoo
[odoo@media-stream admin]$
``` 
4. Membuka PostgreSQL > `psql`
```
[odoo@media-stream admin]$ psql
could not change directory to "/home/admin": Permission denied
psql (12.7)
Type "help" for help.

odoo=#
```
5. Mengkoneksikan dengan database *efarina* > `\c efarina`
```
odoo=# \c efarina
You are now connected to database "efarina" as user "odoo".
efarina=#
```

## 4. Source Code
[https://gitlab.com/dnaomissions/efarina](https://gitlab.com/dnaomissions/efarina)

## 5. User Privileges
```
           nama          |                           keterangan
-------------------------+------------------------------------------------------------------
 Menu Siswa              | Dapat mengakses Menu Siswa
 Menu Pemasukan          | Dapat mengakses Menu Keuangan / Pemasukan
 Menu Pengeluaran        | Dapat mengakses Menu Keuangan / Pengeluaran
 Menu Kelas              | Dapat mengakses Menu Manajemen Kelas / General / Kelas
 Menu Jurusan            | Dapat mengakses Menu Manajemen Kelas / General / Jurusan
 Menu Guru               | Dapat mengakses Menu Manajemen Kelas / General / Guru
 Menu Tingkatan          | Dapat mengakses Menu Manajemen Kelas / General / Tingkatan
 Menu Biaya              | Dapat mengakses Menu Manajemen Kelas / Biaya / Biaya
 Menu Asrama             | Dapat mengakses Menu Manajemen Kelas / Master Data / Asrama
 Report Kas Harian       | Dapat mengakses Menu Laporan / Kas / Kas Harian
 Report Kas Bulanan      | Dapat mengakses Menu Laporan / Kas / Kas Bulanan
 Report Resume           | Dapat mengakses Menu Laporan / Piutang / Resume
 Report Piutang Kelas    | Dapat mengakses Menu Laporan / Piutang / Piutang Kelas
 Report Informasi Siswa  | Dapat mengakses Menu Laporan / Siswa / Informasi Siswa
 Menu Chart of Account   | Dapat mengakses Menu Konfigurasi / Chart of Account
 Menu Bank and Cash      | Dapat mengakses Menu Konfigurasi / Transaksi / Bank and Cash
 Menu Jenis Pendapatan   | Dapat mengakses Menu Konfigurasi / Transaksi / Jenis Pendapatan
 Menu Jenis Pengeluaran  | Dapat mengakses Menu Konfigurasi / Transaksi / Jenis Pengeluaran
 Menu Order              | Dapat mengakses Menu Keuangan / Order
 Menu Semester           | Dapat mengakses Menu Konfigurasi / Settings / Semester 
 Button Orders Set Draft | Memunculkan Button "Set to Draft" pada Form Transaksi
                         | (Pengeluaran & Pemasukan) untuk mengembalikan Status 
                         | dari Confirmed menjadi Draft
 Button Orders Set Done  | Memunculkan Button "Mark as Done" pada Form Transaksi
                         | (Pengeluaran & Pemasukan) untuk mengubah Status 
                         | dari Confirmed menjadi Done
 Super User              | Memunculkan Button "Ambil Kewajiban Kelas" pada Form Kelas 
                         | dan "Ambil Kewajiban Siswa" pada Form Siswa
```

## 6. User Credential
```
    name    |        login        | password
------------+---------------------+-----------------------
 Superadmin | superadmin@mail.com | EfarinaSuperadmin2021
 Admin      | admin@mail.com      | EfarinaAdmin2021
```